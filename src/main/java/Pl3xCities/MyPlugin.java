package Pl3xCities;

import net.pl3x.pl3xcities.Visualizer;
import net.pl3x.pl3xcities.commands.City;
import net.pl3x.pl3xcities.commands.Pl3xCities;
import net.pl3x.pl3xcities.commands.Plot;
import net.pl3x.pl3xcities.helpers.Chunk;
import net.pl3x.pl3xcities.helpers.Location;
import net.pl3x.pl3xcities.runnables.StartMetrics;
import net.pl3x.pl3xlibs.Logger;
import net.pl3x.pl3xlibs.Pl3xLibs;
import net.pl3x.pl3xlibs.configuration.BaseConfig;
import PluginReference.MC_EventInfo;
import PluginReference.MC_Location;
import PluginReference.MC_Player;
import PluginReference.MC_Server;
import PluginReference.PluginBase;
import PluginReference.PluginInfo;

public class MyPlugin extends PluginBase {
	private static MyPlugin instance;
	private MC_Server server;
	private Logger logger;
	private BaseConfig config;
	private Visualizer visualizer;

	public static MyPlugin getInstance() {
		return instance;
	}

	public PluginInfo getPluginInfo() {
		PluginInfo info = new PluginInfo();
		info.name = "Pl3xCities";
		info.version = "0.1-SNAPSHOT";
		info.description = "User ran and operated land management plugin";
		return info;
	}

	@Override
	public void onStartup(MC_Server argServer) {
		server = argServer;
		instance = this;
		init();
		getServer().registerCommand(new Pl3xCities(this));
		getServer().registerCommand(new City(this));
		getServer().registerCommand(new Plot(this));
		Pl3xLibs.getScheduler().scheduleTask(getPluginInfo().name, new StartMetrics(this), 100);
		log(getPluginInfo().name + " v" + getPluginInfo().version + " by BillyGalbreath and Furyan is now enabled.");
	}

	@Override
	public void onShutdown() {
		log("Plugin disabled.");
	}

	private void init() {
		getConfig();
		visualizer = new Visualizer(this);
	}

	private void disable() {
		Pl3xLibs.getScheduler().cancelAllRunnables(getPluginInfo().name);
		visualizer = null;
		config = null;
		logger = null;
	}

	public void reload() {
		disable();
		init();
	}

	public MC_Server getServer() {
		return server;
	}

	public BaseConfig getConfig() {
		if (config == null) {
			config = new BaseConfig(MyPlugin.class, getPluginInfo(), "", "config.ini");
		}
		return config;
	}

	public Visualizer getVisualizer() {
		return visualizer;
	}

	public void log(String message) {
		getLogger().info(message);
	}

	public void debug(String message) {
		if (getConfig().getBoolean("debug-mode", false)) {
			getLogger().debug(message);
		}
	}

	private Logger getLogger() {
		if (logger == null) {
			logger = Pl3xLibs.getLogger(getPluginInfo());
		}
		return logger;
	}

	@Override
	public void onAttemptPlayerMove(MC_Player player, MC_Location locFrom, MC_Location locTo, MC_EventInfo ei) {
		if (locFrom.isSameBlockLocationAs(locTo)) {
			return;
		}
		Location from = new Location(locFrom);
		Location to = new Location(locTo);
		Chunk chunkFrom = from.getBlock().getChunk();
		Chunk chunkTo = to.getBlock().getChunk();
		if (chunkFrom.getX() == chunkTo.getX() && chunkFrom.getZ() == chunkTo.getZ()) {
			return;
		}
		log("NEW CHUNK!!!!!!!!!");
		getVisualizer().update(player.getUUID(), from, to);
	}
}
