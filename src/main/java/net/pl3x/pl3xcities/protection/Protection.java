package net.pl3x.pl3xcities.protection;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

import net.pl3x.pl3xcities.helpers.Location;

public class Protection {
	private UUID owner;
	private Set<UUID> members = new HashSet<UUID>();
	private double price = 0D;
	private boolean forsale = false;
	private Location spawn;

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public boolean isForSale() {
		return forsale;
	}

	public void setForSale(boolean forsale) {
		this.forsale = forsale;
	}

	public Location getSpawn() {
		return spawn;
	}

	public void setSpawn(Location spawn) {
		this.spawn = spawn;
	}

	public UUID getOwner() {
		return owner;
	}

	public void setOwner(UUID owner) {
		this.owner = owner;
	}

	public boolean isOwner(UUID uuid) {
		return getOwner().equals(uuid);
	}

	public Set<UUID> getMembers() {
		return members;
	}

	public void setMembers(Set<UUID> members) {
		this.members = members;
	}

	public boolean isMember(UUID uuid) {
		return getMembers().contains(uuid);
	}

	public void addMember(UUID uuid) {
		Set<UUID> members = getMembers();
		if (members.contains(uuid)) {
			return;
		}
		members.add(uuid);
		setMembers(members);
	}

	public void removeMember(UUID uuid) {
		Set<UUID> members = getMembers();
		if (!members.contains(uuid)) {
			return;
		}
		members.remove(uuid);
		setMembers(members);
	}

	public boolean canBuild(UUID uuid) {
		return isOwner(uuid) || isMember(uuid);
	}
}
