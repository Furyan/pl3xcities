package net.pl3x.pl3xcities.protection;

import java.util.HashSet;
import java.util.Set;

import net.pl3x.pl3xcities.helpers.Chunk;
import net.pl3x.pl3xcities.helpers.Location;

public class City {
	private Set<Chunk> chunks = new HashSet<Chunk>();

	public Set<Chunk> getChunks() {
		return chunks;
	}

	public void setChunks(Set<Chunk> chunks) {
		this.chunks = chunks;
	}

	public void addChunk(Chunk chunk) {
		Set<Chunk> chunks = getChunks();
		if (chunks.contains(chunk)) {
			return;
		}
		chunks.add(chunk);
		setChunks(chunks);
	}

	public void removeChunk(Chunk chunk) {
		Set<Chunk> chunks = getChunks();
		if (!chunks.contains(chunk)) {
			return;
		}
		chunks.remove(chunk);
		setChunks(chunks);
	}

	public Chunk getChunk(Location location) {
		return getChunk(location.getBlock().getChunk());
	}

	public Chunk getChunk(Chunk chunk) {
		for (Chunk chnk : getChunks()) {
			if (!chnk.getWorld().equals(chunk.getWorld())) {
				continue;
			}
			if (chnk.getX() != chunk.getX()) {
				continue;
			}
			if (chnk.getZ() != chunk.getZ()) {
				continue;
			}
			return chnk;
		}
		return null;
	}

	public String getArea() {
		return Integer.toString(getChunks().size() * (16 * 16));
	}
}
