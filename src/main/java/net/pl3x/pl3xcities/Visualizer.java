package net.pl3x.pl3xcities;

import java.util.HashMap;
import java.util.UUID;

import net.pl3x.pl3xcities.helpers.Location;
import Pl3xCities.MyPlugin;

public class Visualizer {
	private HashMap<UUID, VChunk> data = new HashMap<UUID, VChunk>();

	public Visualizer(MyPlugin plugin) {
	}

	public void enable(UUID uuid, Location location) {
		if (isEnabled(uuid)) {
			return;
		}
		VChunk vChunk = new VChunk(location);
		data.put(uuid, vChunk);

		vChunk.visualize(uuid, findType(uuid, location));
	}

	public void disable(UUID uuid) {
		if (!isEnabled(uuid)) {
			return;
		}
		data.get(uuid).unvisualize(uuid);
		data.remove(uuid);
	}

	public boolean isEnabled(UUID uuid) {
		return data.containsKey(uuid);
	}

	public void update(UUID uuid, Location from, Location to) {
		if (!isEnabled(uuid)) {
			//return;
		}
		if (data.get(uuid) != null) {
			data.get(uuid).unvisualize(uuid);
		}
		VChunk vChunk = new VChunk(to);
		data.put(uuid, vChunk);
		vChunk.visualize(uuid, findType(uuid, to));
	}

	public OwnedChunkType findType(UUID uuid, Location location) {
		OwnedChunkType type = OwnedChunkType.UNCLAIMED;
		return type;
	}
}
