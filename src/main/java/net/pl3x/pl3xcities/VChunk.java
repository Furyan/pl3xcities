package net.pl3x.pl3xcities;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import joebkt.IntegerCoordinates;
import joebkt.Packet_BlockChange;
import net.pl3x.pl3xcities.helpers.Block;
import net.pl3x.pl3xcities.helpers.Location;
import net.pl3x.pl3xlibs.Material;
import net.pl3x.pl3xlibs.Pl3xLibs;
import Pl3xCities.MyPlugin;
import PluginReference.MC_Block;
import PluginReference.MC_Player;
import WrapperObjects.BlockWrapper;
import WrapperObjects.WorldWrapper;
import WrapperObjects.Entities.PlayerWrapper;

public class VChunk {
	private final Set<Location> visualizeBlocks = new HashSet<Location>();

	public VChunk(Location location) {
		Location loc1 = location.getBlock().getChunk().getBlock(0, 0, 0).getLocation();
		Location loc2 = new Location(loc1.getWorld(), loc1.getX() + 1, loc1.getY(), loc1.getZ());
		Location loc3 = new Location(loc1.getWorld(), loc1.getX(), loc1.getY(), loc1.getZ() + 1);
		Location loc4 = new Location(loc1.getWorld(), loc1.getX() + 15, loc1.getY(), loc1.getZ());
		Location loc5 = new Location(loc4.getWorld(), loc4.getX() - 1, loc4.getY(), loc4.getZ());
		Location loc6 = new Location(loc4.getWorld(), loc4.getX(), loc4.getY(), loc4.getZ() + 1);
		Location loc7 = new Location(loc1.getWorld(), loc1.getX() + 15, loc1.getY(), loc1.getZ() + 15);
		Location loc8 = new Location(loc7.getWorld(), loc7.getX() - 1, loc7.getY(), loc7.getZ());
		Location loc9 = new Location(loc7.getWorld(), loc7.getX(), loc7.getY(), loc7.getZ() - 1);
		Location loc10 = new Location(loc1.getWorld(), loc1.getX(), loc1.getY(), loc1.getZ() + 15);
		Location loc11 = new Location(loc10.getWorld(), loc10.getX() + 1, loc10.getY(), loc10.getZ());
		Location loc12 = new Location(loc10.getWorld(), loc10.getX(), loc10.getY(), loc10.getZ() - 1);

		visualizeBlocks.add(getHighestVisual(loc1));
		visualizeBlocks.add(getHighestVisual(loc2));
		visualizeBlocks.add(getHighestVisual(loc3));
		visualizeBlocks.add(getHighestVisual(loc4));
		visualizeBlocks.add(getHighestVisual(loc5));
		visualizeBlocks.add(getHighestVisual(loc6));
		visualizeBlocks.add(getHighestVisual(loc7));
		visualizeBlocks.add(getHighestVisual(loc8));
		visualizeBlocks.add(getHighestVisual(loc9));
		visualizeBlocks.add(getHighestVisual(loc10));
		visualizeBlocks.add(getHighestVisual(loc11));
		visualizeBlocks.add(getHighestVisual(loc12));
	}

	public void visualize(UUID uuid, OwnedChunkType chunkType) {
		Material material;
		int data;
		switch (chunkType) {
			case CITY_MINE:
				material = Material.WOOL;
				data = 1;
				break;
			case CITY_OTHER:
				material = Material.WOOL;
				data = 2;
				break;
			case PLOT_MINE:
				material = Material.WOOL;
				data = 3;
				break;
			case PLOT_OTHER:
				material = Material.WOOL;
				data = 4;
				break;
			case UNCLAIMED:
			default:
				material = Material.WOOL;
				data = 5;
		}
		MC_Player player = Pl3xLibs.getPlayer(uuid);
		for (Location location : visualizeBlocks) {
			sendBlockChange(player, location, material, data);
		}
	}

	public void unvisualize(UUID uuid) {
		MC_Player player = Pl3xLibs.getPlayer(uuid);
		for (Location location : visualizeBlocks) {
			Block block = location.getBlock();
			sendBlockChange(player, location, block.getType(), block.getHandle().getSubtype());
		}
	}

	public Location getHighestVisual(Location location) {
		int ceiling = MyPlugin.getInstance().getServer().getMaxBuildHeight() - 1;
		int x = location.getBlockX();
		int z = location.getBlockZ();
		List<Material> ignore = new ArrayList<Material>();
		for (String matName : ignoreList()) {
			Material mat = Material.matchMaterial(matName);
			if (mat != null) {
				ignore.add(mat);
			}
		}
		for (int y = ceiling; y > 0; y--) {
			Location newLocation = new Location(location.getWorld(), x, y, z);
			Material material = newLocation.getBlock().getType();
			if (ignore.contains(material)) {
				// ignore certain blocks specified in config.yml
				continue;
			}
			return newLocation;
		}
		return new Location(location.getWorld(), x, 0, z);
	}

	private List<String> ignoreList() {
		List<String> ignore = new ArrayList<String>();
		ignore.add("AIR");
		ignore.add("SAPLING");
		ignore.add("POWERED_RAIL");
		ignore.add("DETECTOR_RAIL");
		ignore.add("LONG_GRASS");
		ignore.add("DEAD_BUSH");
		ignore.add("YELLOW_FLOWER");
		ignore.add("RED_ROSE");
		ignore.add("BROWN_MUSHROOM");
		ignore.add("RED_MUSHROOM");
		ignore.add("TORCH");
		ignore.add("FIRE");
		ignore.add("REDSTONE_WIRE");
		ignore.add("CROPS");
		ignore.add("LADDER");
		ignore.add("RAILS");
		ignore.add("LEVER");
		ignore.add("REDSTONE_TORCH_OFF");
		ignore.add("REDSTONE_TORCH_ON");
		ignore.add("STONE_BUTTON");
		ignore.add("SNOW");
		ignore.add("SUGAR_CANE_BLOCK");
		ignore.add("PORTAL");
		ignore.add("DIODE_BLOCK_OFF");
		ignore.add("DIODE_BLOCK_ON");
		ignore.add("PUMPKIN_STEM");
		ignore.add("MELON_STEM");
		ignore.add("VINE");
		ignore.add("WATER_LILY");
		ignore.add("NETHER_WARTS");
		ignore.add("ENDER_PORTAL");
		ignore.add("COCOA");
		ignore.add("TRIPWIRE_HOOK");
		ignore.add("TRIPWIRE");
		ignore.add("FLOWER_POT");
		ignore.add("CARROT");
		ignore.add("POTATO");
		ignore.add("WOOD_BUTTON");
		ignore.add("SKULL");
		ignore.add("REDSTONE_COMPARATOR_OFF");
		ignore.add("REDSTONE_COMPARATOR_ON");
		ignore.add("ACTIVATOR_RAIL");
		ignore.add("CARPET");
		ignore.add("DOUBLE_PLANT");
		ignore.add("LOG");
		ignore.add("LOG_2");
		ignore.add("LEAVES");
		ignore.add("LEAVES_2");
		ignore.add("DISPENSER");
		ignore.add("NOTE_BLOCK");
		ignore.add("BED_BLOCK");
		ignore.add("PISTON_MOVING_PIECE");
		ignore.add("MOB_SPAWNER");
		ignore.add("CHEST");
		ignore.add("WORKBENCH");
		ignore.add("FURNACE");
		ignore.add("BURNING_FURNACE");
		ignore.add("SIGN_POST");
		ignore.add("WOODEN_DOOR");
		ignore.add("WALL_SIGN");
		ignore.add("STONE_PLATE");
		ignore.add("WOOD_PLATE");
		ignore.add("CACTUS");
		ignore.add("FENCE");
		ignore.add("PUMPKIN");
		ignore.add("JACK_O_LANTERN");
		ignore.add("CAKE_BLOCK");
		ignore.add("LOCKED_CHEST");
		ignore.add("TRAP_DOOR");
		ignore.add("MONSTER_EGGS");
		ignore.add("IRON_FENCE");
		ignore.add("THIN_GLASS");
		ignore.add("MELON_BLOCK");
		ignore.add("FENCE_GATE");
		ignore.add("NETHER_FENCE");
		ignore.add("ENCHANTMENT_TABLE");
		ignore.add("BREWING_STAND");
		ignore.add("CAULDRON");
		ignore.add("DRAGON_EGG");
		ignore.add("ENDER_CHEST");
		ignore.add("COMMAND");
		ignore.add("BEACON");
		ignore.add("COBBLE_WALL");
		ignore.add("ANVIL");
		ignore.add("TRAPPED_CHEST");
		ignore.add("GOLD_PLATE");
		ignore.add("IRON_PLATE");
		ignore.add("DAYLIGHT_DETECTOR");
		ignore.add("HOPPER");
		ignore.add("DROPPER");
		return ignore;
	}

	private void sendBlockChange(MC_Player player, Location loc, Material material, int data) {
		IntegerCoordinates coords = new IntegerCoordinates(loc.getBlockX(), loc.getBlockY(), loc.getBlockZ());
		Packet_BlockChange packet = new Packet_BlockChange(((WorldWrapper) loc.getWorld().getHandle()).world, coords);

		MC_Block block = player.getWorld().getBlockFromName(material.getName());
		block.setSubtype(data);

		packet.b = ((BlockWrapper) block).m_blockState;
		packet.a = coords;
		((PlayerWrapper) player).plr.plrConnection.sendPacket(packet);
	}
}
