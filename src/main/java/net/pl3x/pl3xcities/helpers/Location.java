package net.pl3x.pl3xcities.helpers;

import PluginReference.MC_Location;

public class Location {
	private double x;
	private double y;
	private double z;
	private float yaw;
	private float pitch;
	private World world;

	public Location(World world, double x, double y, double z, float yaw, float pitch) {
		this.world = world;
		this.x = x;
		this.y = y;
		this.z = z;
		this.yaw = yaw;
		this.pitch = pitch;
	}

	public Location(World world, double x, double y, double z) {
		this(world, x, y, z, 0, 0);
	}

	public Location(World world, int x, int y, int z) {
		this(world, (double) x, (double) y, (double) z);
	}

	public Location(MC_Location loc) {
		this(new World(loc.dimension), loc.x, loc.y, loc.z, loc.yaw, loc.pitch);
	}

	public double getX() {
		return x;
	}

	public void setX(double x) {
		this.x = x;
	}

	public double getY() {
		return y;
	}

	public void setY(double y) {
		this.y = y;
	}

	public double getZ() {
		return z;
	}

	public void setZ(double z) {
		this.z = z;
	}

	public float getYaw() {
		return yaw;
	}

	public void setYaw(float yaw) {
		this.yaw = yaw;
	}

	public float getPitch() {
		return pitch;
	}

	public void setPitch(float pitch) {
		this.pitch = pitch;
	}

	public int getBlockX() {
		return (int) Math.floor(x);
	}

	public int getBlockY() {
		return (int) Math.floor(y);
	}

	public int getBlockZ() {
		return (int) Math.floor(z);
	}

	public Block getBlock() {
		return world.getBlockAt(this);
	}

	public World getWorld() {
		return world;
	}

	public Location clone(Location loc) {
		return new Location(loc.getWorld(), loc.getX(), loc.getY(), loc.getZ());
	}

	public Boolean equals(Location loc) {
		return loc.x == x && loc.y == y && loc.z == z && loc.world.equals(world);
	}

	public double distanceTo(Location loc) {
		if (loc.getWorld().equals(getWorld())) {
			return 8.988465674311579E+307D;
		}
		double dx = this.x - loc.x;
		double dy = this.y - loc.y;
		double dz = this.z - loc.z;
		return Math.sqrt(dx * dx + dy * dy + dz * dz);
	}
}
