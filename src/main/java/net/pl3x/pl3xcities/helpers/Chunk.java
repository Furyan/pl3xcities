package net.pl3x.pl3xcities.helpers;

public class Chunk {
	private World world;
	private int x;
	private int z;

	@SuppressWarnings("unused")
	private Chunk() {
	}

	public Chunk(World world, int x, int z) {
		this.x = x;
		this.z = z;
		this.world = world;
	}

	public Chunk(Location loc) {
		this(loc.getWorld(), loc.getBlockX() >> 4, loc.getBlockZ() >> 4);
	}

	public int getX() {
		return x;
	}

	public int getZ() {
		return z;
	}

	public World getWorld() {
		return world;
	}

	public Block getBlock(int x, int y, int z) {
		return new Block(this, (getX() << 4) | (x & 0xF), y & 0xFF, (getZ() << 4) | (z & 0xF));
	}
}
