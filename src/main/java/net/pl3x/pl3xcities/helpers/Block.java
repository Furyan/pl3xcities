package net.pl3x.pl3xcities.helpers;

import net.pl3x.pl3xlibs.Material;
import PluginReference.MC_Block;

public class Block {
	private Chunk chunk;
	private int x;
	private int y;
	private int z;

	public Block(Chunk chunk, int x, int y, int z) {
		this.setX(x);
		this.setY(y);
		this.setZ(z);
		this.setChunk(chunk);
	}

	public World getWorld() {
		return chunk.getWorld();
	}

	public Chunk getChunk() {
		return chunk;
	}

	public void setChunk(Chunk chunk) {
		this.chunk = chunk;
	}

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public int getZ() {
		return z;
	}

	public void setZ(int z) {
		this.z = z;
	}

	public Material getType() {
		return Material.matchMaterial(Integer.toString(getTypeId()));
	}

	public int getTypeId() {
		return getHandle().getId();
	}

	public Location getLocation() {
		return new Location(chunk.getWorld(), x, y, z);
	}

	public MC_Block getHandle() {
		return chunk.getWorld().getHandle().getBlockAt(x, y, z);
	}
}
