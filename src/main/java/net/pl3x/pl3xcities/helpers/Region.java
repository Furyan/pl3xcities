package net.pl3x.pl3xcities.helpers;

import PluginReference.MC_Location;

public class Region {
	private int minX;
	private int minY;
	private int minZ;
	private int maxX;
	private int maxY;
	private int maxZ;
	private World world;

	public Region(Location loc1, Location loc2) {
		this.minX = Math.min(loc1.getBlockX(), loc2.getBlockX());
		this.minY = Math.min(loc1.getBlockY(), loc2.getBlockY());
		this.minZ = Math.min(loc1.getBlockZ(), loc2.getBlockZ());
		this.maxX = Math.max(loc1.getBlockX(), loc2.getBlockX());
		this.maxY = Math.max(loc1.getBlockY(), loc2.getBlockY());
		this.maxZ = Math.max(loc1.getBlockZ(), loc2.getBlockZ());
		this.world = loc1.getWorld();
	}

	public Region(MC_Location loc1, MC_Location loc2) {
		this(new Location(loc1), new Location(loc2));
	}

	public int getMinX() {
		return minX;
	}

	public int getMinY() {
		return minY;
	}

	public int getMinZ() {
		return minZ;
	}

	public int getMaxX() {
		return maxX;
	}

	public int getMaxY() {
		return maxY;
	}

	public int getMaxZ() {
		return maxZ;
	}

	public World getWorld() {
		return world;
	}

	public int getWidthX() {
		return maxX - minX;
	}

	public int getWidthZ() {
		return maxZ - minZ;
	}

	public int getHeight() {
		return maxY - minY;
	}

	public int getVolume() {
		return getHeight() * getWidthX() * getWidthZ();
	}
}
