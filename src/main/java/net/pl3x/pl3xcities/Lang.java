package net.pl3x.pl3xcities;

import net.pl3x.pl3xlibs.configuration.BaseConfig;
import Pl3xCities.MyPlugin;

public enum Lang {
	ERROR_NO_PERM("error-no-permission", "&4You do not have permission for that command!"),
	ERROR_PLAYER_NOT_FOUND("error-player-not-found", "&4Player not found!"),
	ERROR_UNKNOWN_MISSING_SUBCOMMAND("error-unknown-missing-subcommand", "&4Unknown or missing sub-command!"),
	ERROR_INVALID_COMMAND_STRUCTURE("error-invalid-command-structure", "&4Invalid command structure!"),
	RELOAD("reload", "&dPl3xCities reloaded."),

	HELP_PL3XCITIES("help-pl3xcities", "Manage the plugin"),
	HELP_PL3XCITIES_RELOAD("help-pl3xcities-reload", "Reload the plugin"),
	HELP_CITY("help-city", "Manage a city"),
	HELP_PLOT("help-plot", "Manage a plot"),
	;

	private String key;
	private String def;

	private static BaseConfig config;

	private Lang(String key, String def) {
		this.key = key;
		this.def = def;
		init();
	}

	private static void init() {
		config = new BaseConfig(MyPlugin.class, MyPlugin.getInstance().getPluginInfo(), "", MyPlugin.getInstance().getConfig().get("language-file", "lang-en.ini"));
		config.load();
	}

	public static void refreshAll() {
		config = null;
		init();
	}

	public String get() {
		String value = config.get(key, def);
		if (value == null) {
			// This shouldn't happen
			value = "&c[missing lang data]";
		}
		return value;
	}
}
