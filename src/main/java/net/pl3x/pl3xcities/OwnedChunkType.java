package net.pl3x.pl3xcities;

public enum OwnedChunkType {
	CITY_MINE,
	CITY_OTHER,
	PLOT_MINE,
	PLOT_OTHER,
	UNCLAIMED;
}
