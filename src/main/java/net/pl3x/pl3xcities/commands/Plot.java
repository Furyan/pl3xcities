package net.pl3x.pl3xcities.commands;

import net.pl3x.pl3xcities.Lang;
import net.pl3x.pl3xlibs.commands.BaseCommand;
import Pl3xCities.MyPlugin;

public class Plot extends BaseCommand {
	public Plot(MyPlugin plugin) {
		super("plot", Lang.HELP_PLOT.get(), "pl3xcities.command.plot", null);
	}
}
