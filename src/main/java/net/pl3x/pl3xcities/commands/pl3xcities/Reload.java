package net.pl3x.pl3xcities.commands.pl3xcities;

import java.util.LinkedList;
import java.util.List;

import net.pl3x.pl3xcities.Lang;
import net.pl3x.pl3xlibs.Pl3xLibs;
import net.pl3x.pl3xlibs.commands.Command;
import Pl3xCities.MyPlugin;
import PluginReference.MC_Player;

public class Reload extends Command {
	private MyPlugin plugin;

	public Reload(MyPlugin plugin) {
		super("reload", Lang.HELP_PL3XCITIES_RELOAD.get(), "pl3xcities.command.pl3xcities.reload", null);
		this.plugin = plugin;
	}

	@Override
	public List<String> getTabCompletionList(LinkedList<String> args, MC_Player player) {
		return null;
	}

	@Override
	public void handleCommand(LinkedList<String> args, MC_Player player) throws Exception {
		plugin.reload();
		Pl3xLibs.sendMessage(player, Lang.RELOAD.get());
	}
}
