package net.pl3x.pl3xcities.commands;

import net.pl3x.pl3xcities.Lang;
import net.pl3x.pl3xcities.commands.pl3xcities.Reload;
import net.pl3x.pl3xlibs.commands.BaseCommand;
import Pl3xCities.MyPlugin;

public class Pl3xCities extends BaseCommand {
	public Pl3xCities(MyPlugin plugin) {
		super("pl3xcities", Lang.HELP_PL3XCITIES.get(), "pl3xcities.command.pl3xcities", null);
		registerSubcommand(new Reload(plugin));
	}
}
